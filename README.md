# TeamMod

Enables moderation tools in private teams.  See the [Stack Apps post](https://stackapps.com/q/8949/79306) for more info, to leave feedback, comments, suggestions, bug reports or to upvote if you like it!
